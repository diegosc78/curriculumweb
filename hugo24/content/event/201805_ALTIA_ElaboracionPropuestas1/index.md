---
title: Introducción a la elaboración de propuestas. Sesión 1
event: Formación impartida en Altia
event_url: ""

location: Altia
address:
  street: ""
  city: Vigo
  region: Pontevedra
  postcode: ""
  country: ""

summary: Seminario interno de formación estratégico impartido a compañeros.
abstract: "En este seminario se pretende difundir un conjunto de buenas prácticas adquiridas con la experiencia en relación a la elaboración de propuestas comerciales para nuestros clientes así como aportar técnicas, herramientas y recursos prácticos y valiosos para afrontar esta labor."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2018-04-26T10:00:00Z"
date_end: "2018-04-26T14:00:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2020-01-01T00:00:00Z"

authors: []
tags:
- Desarrollo de negocio

# Is this a featured talk? (true/false)
featured: false

image:
  caption: 'Altia Oleiros'
  focal_point: Right

links: []
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
#- internal-project

# Enable math on this page?
math: false
---
