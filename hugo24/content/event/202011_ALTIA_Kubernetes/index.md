---
title: Kubernetes. Automatización de la implementación, el escalado y la administración de aplicaciones en contenedores
event: Formación impartida en Altia
event_url: ""

location: Altia
address:
  street: ""
  city: Oleiros
  region: Coruña
  postcode: ""
  country: ""

summary: Curso de formación interno estratégico impartido a compañeros.
abstract: "En este curso se muestra cómo contenerizar aplicaciones, desplegarlas, administrarlas y escalarlas en un entorno de orquestación de contenedores como kubernetes. En este curso actúo como formador (de una parte) y como alumno (de otra)."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2020-11-30T08:00:00Z"
date_end: "2020-12-04T15:00:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2020-12-04T00:00:00Z"

authors: []
tags:
- DevOps
- Contenedores

# Is this a featured talk? (true/false)
featured: false

image:
  caption: 'Altia Oleiros'
  focal_point: Right

links: []
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
#- internal-project

# Enable math on this page?
math: false
---
