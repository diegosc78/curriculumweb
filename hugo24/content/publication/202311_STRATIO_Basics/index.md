---
title: "Stratio Augmented Data Fabric Basics Certification"
authors:
- admin
date: "2023-11-07T00:00:00Z"
doi: ""
draft: false

# Schedule page publish date (NOT publication's date).
publishDate: "2023-11-07T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: []

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: "Stratio Data Fabric Basics"

abstract: Stratio Augmented Data Fabric Basics Certification

# Summary. An optional shortened abstract.
summary: Stratio Augmented Data Fabric Basics Certification - Stratio - 2023

tags:
- Big data
- IA
featured: false

links:
- name: Entidad organizadora
  url: "https://www.stratio.com/"
url_pdf: ''
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ''
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

## Datos del curso

* Organismo: Stratio
* Fecha: 2023-11
* Duración: 16h
* Modalidad: teleformación
* Diploma acreditativo: disponible

## Temario

* Introducción general a la plataforma Stratio OBJ-BAS-1
* Introducción Augmented Data Governance OBJ-2-GOV
* Introducción Entorno Analítico Rocket OBJ-3-ROCKET
* Examen certificación: Stratio Augmented Data Fabric Basics (13.2)
