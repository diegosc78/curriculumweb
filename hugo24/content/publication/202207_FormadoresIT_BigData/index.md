---
title: "Big Data y Transformación Digital"
authors:
- admin
date: "2022-07-26T00:00:00Z"
doi: ""
draft: false

# Schedule page publish date (NOT publication's date).
publishDate: "2022-07-26T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: []

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: "Big data"

abstract: Big Data y Transformación Digital

# Summary. An optional shortened abstract.
summary: Big Data y Transformación Digital - FormadoresIT - 2022

tags:
- Big data
featured: false

links:
- name: Entidad organizadora
  url: "https://formadoresit.es/"
url_pdf: ''
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ''
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

## Datos del curso

* Organismo: Formadores IT
* Fecha: 2022-06
* Duración: 40h
* Modalidad: telepresencial
* Diploma acreditativo: disponible

## Temario

* De los datos a las decisiones estratégicas. El mundo del dato
* Data Management
* Sistemas de información: Business Intelligence. ¿Por qué aparece Big Data? ¿Qué significa?
* Arquitecturas Big Data
* Visualización y toma de decisiones
* Big Data & Analytics: Disciplinas científicas
* Big Data & Analytics: Ámbitos de aplicación
* Transformación Digital e IoT
* Liderazgo y Gestión de proyectos de dato
* Protección de datos
