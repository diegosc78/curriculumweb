---
title: "Programa avanzado en Agile Project Management. SCRUM"
authors:
- admin
date: "2024-05-27T00:00:00Z"
doi: ""
draft: false

# Schedule page publish date (NOT publication's date).
publishDate: "2024-05-27T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: []

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: "Agile Project Management. SCRUM"

abstract: Programa avanzado en Agile Project Management. SCRUM

# Summary. An optional shortened abstract.
summary: Programa avanzado en Agile Project Management. SCRUM - Nred Soluciones Formativas - 2024

tags:
- Agile
- Gestión de proyectos
featured: false

links:
- name: "Organizadora: Consellería de emprego, comercio e emigración"
  url: "https://conselleriaemprego.xunta.gal/"
- name: "Entidad impartidora: Nred"
  url: "https://www.n-red.es/"
url_pdf: ''
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ''
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

## Datos del curso

* Organismo: Consellería de emprego, comercio e emigración
* Fecha: 2024-05
* Duración: 150h
* Modalidad: teleformación
* Diploma acreditativo: disponible

## Temario

* Gestión de proyectos
  * Herramientas para optimizar la gestión de proyectos software
  * Gestión ágil con Scrum y Kanban
  * Gestión de multiproyectos
  * Design Thinking
* Comunicación empresarial
  * Habilidades de comunicación
  * Negociación avanzada
  * Liderazgo y transformación
  * Gestión del talento y nuevos modelos de organización
  * Gestión de equipos de alto rendimiento
* Estrategia y negocio
  * Business Intelligence
  * Customer experience
  * Estrategias digitales
  * Nuevos entornos
  * Value Proposition Design
  * Business Models Generation
  * Complex Sales Strateg
  * La metodología Scrum aplicada a los nuevos modelos de negocio
