---
title: "Design Thinking para tod@s"
authors:
- admin
date: "2018-03-08T00:00:00Z"
doi: ""
draft: false

# Schedule page publish date (NOT publication's date).
publishDate: "2018-03-08T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: []

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: "Design Thinking"

abstract: Design Thinking para tod@s

# Summary. An optional shortened abstract.
summary: Design Thinking para tod@s - UPM - 2018

tags:
- Design Thinking
featured: false

links:
- name: Entidad organizadora
  url: "https://www.upm.es/"
url_pdf: ''
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ''
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

## Datos del curso

* Organismo: Universidad Politécnica de Madrid (a través de MiriadaX)
* Fecha: 2018-03
* Duración: 20h
* Modalidad: teleformación
* Diploma acreditativo: disponible
