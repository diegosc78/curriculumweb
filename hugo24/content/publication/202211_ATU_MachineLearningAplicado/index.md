---
title: "Inteligencia artificial aplicada a la empresa"
authors:
- admin
date: "2022-11-17T00:00:00Z"
doi: ""
draft: false

# Schedule page publish date (NOT publication's date).
publishDate: "2022-11-17T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: []

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: "ML Python"

abstract: Inteligencia artificial aplicada a la empresa

# Summary. An optional shortened abstract.
summary: Inteligencia artificial aplicada a la empresa - ATU - 2022

tags:
- IA
featured: false

links:
- name: Enlace al curso
  url: "https://cursosatu.grupoatu.com/cursos/inteligencia-artificial-aplicada-a-la-empresa/"
url_pdf: ''
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ''
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

## Datos del curso

* Organismo: ATU
* Fecha: 2022-11
* Duración: 250h
* Modalidad: teleformación
* Diploma acreditativo: disponible

## Temario

1. INTRODUCCIÓN A LA IA.
    1. Definición. Historia.
    2. Ramas de la IA. Algoritmos.
    3. Machine/Deep Learning.
    4. Big data: el cambio en la IA.
2. ALGORITMOS DE IA.
    1. Machine Learning: modelos supervisados.
    2. Machine learning: modelos no supervisados.
    3. Aprendizaje por refuerzo.
    4. Modelos profundos (Deep learning).
    5. Ejemplos con Weka/Orange.
3. APLICACIONES EN LA EMPRESA.
    1. People Analytics.
    2. Predicción: stocks, demandas, comportamientos.
    3. Segmentación: análisis de oferta. Identificar tendencias.
    4. Fidelización de clientes usando aprendizaje reforzado.
    5. Recomendadores web.
    6. Mejora de procesos.
