---
title: "Certificado profesional en Gestión de proyectos de Google"
authors:
- admin
date: "2023-11-24T00:00:00Z"
doi: ""
draft: false

# Schedule page publish date (NOT publication's date).
publishDate: "2023-11-24T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: []

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: "Gestión de proyectos"

abstract: Certificado profesional en Gestión de proyectos de Google

# Summary. An optional shortened abstract.
summary: Certificado profesional en Gestión de proyectos de Google - Coursera - 2023

tags:
- Gestión de proyectos
- PMP
featured: false

links:
- name: Entidad organizadora
  url: "https://google.com/"
- name: Curso en la plataforma de teleformación
  url: "https://www.coursera.org/programs/becas-google-fundae-sepe-g-proyectos-48izs/professional-certificates/gestion-de-proyectos-de-google?collectionId=rnfNZ"
url_pdf: ''
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ''
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

## Datos del curso

* Organismo: Google
* Fecha: 2023-11
* Duración: 120h
* Modalidad: teleformación
* Diploma acreditativo: disponible

## Temario

* Fundamentos de la gestión de proyectos (15 horas)
* Iniciación del proyecto: Cómo iniciar un proyecto exitoso (18 horas)
* Planificación del proyecto: reunir todos los elementos (25 horas)
* Ejecución del proyecto: Ejecutar el proyecto (23 horas)
* Gestión de proyectos con la metodología Agile (22 horas)
* Proyecto final: Aplicación de la gestión de proyectos en el mundo real (32 horas)
