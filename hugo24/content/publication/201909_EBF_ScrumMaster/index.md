---
title: "Scrum Manager"
authors:
- admin
date: "2019-09-05T00:00:00Z"
doi: ""
draft: false

# Schedule page publish date (NOT publication's date).
publishDate: "2019-09-05T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: []

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: "Scrum Manager"

abstract: Scrum Manager

# Summary. An optional shortened abstract.
summary: Scrum Manager - EBF - 2019

tags:
- Agile
- Gestión de proyectos
featured: false

links:
- name: Entidad organizadora
  url: "https://ebf.com.es/"
url_pdf: ''
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ''
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

## Datos del curso

* Organismo: European Business Factory
* Fecha: 2019-09
* Duración: 16h
* Modalidad: presencial
* Diploma acreditativo: disponible

## Temario

* Introducción.
* Descripción de Scrum y de los elementos que lo componen.
* Pila del producto (product backlog).
* Planificación del sprint.
* Pila del sprint (sprint backlog).
* El incremento.
* Roles y responsabilidades.
* Reunión de revisión del sprint.
* Métrica, estimaciones y velocidad.
* Las unidades.
* Gráfico de avance (burn down).
* Gráfico de producto (burn up).
* Estimación de póquer.
* Metodologías de gestión de proyectos.
* Incremento iterativo e incremento continuo.
* Conceptos y patrones de gestión de proyectos.
* Personas, procesos y tecnología.
* Responsabilidades de Scrum a nivel de gestión.
* Trabajando con tableros kanban.
* Muda, Mura y Muri, consejos para ajustar el flujo de tareas.
* Scrum Experience: Simulación de Scrum basada en el juego.
