---
title: "Machine learning aplicado usando Python"
authors:
- admin
date: "2022-12-02T00:00:00Z"
doi: ""
draft: false

# Schedule page publish date (NOT publication's date).
publishDate: "2022-12-02T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: []

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: "ML Python"

abstract: Machine learning aplicado usando Python

# Summary. An optional shortened abstract.
summary: Machine learning aplicado usando Python - CEINPRO - 2022

tags:
- IA
featured: false

links:
- name: Entidad formadora
  url: "https://www.ceinpro.es/"
url_pdf: ''
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ''
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

## Datos del curso

* Organismo: CEINPRO
* Fecha: 2022-12
* Duración: 150h
* Modalidad: teleformación
* Diploma acreditativo: disponible

## Temario

1. INTRODUCCIÓN AL CURSO
    1. Introducción al Python
    2. Librería de Python para Machine Learning.
    3. Machine Learning. Introducción.
2. APRENDIZAJE SUPERVISADO
    1. Definición y aplicaciones.
    2. Medidas de rendimiento.
    3. Modelos lineales
    4. Modelos supervisados de ML: árboles, SVM, redes neuronales.
    5. Combinación de modelos. Random Forest.
3. APRENDIZAJE NO SUPERVISADO
    1. Definición y aplicaciones.
    2. Medidas de rendimiento.
    3. Clustering. Tipos
    4. Biclustering
    5. Manifolds. Reducción de la dimensionalidad
    6. Análisis de la cesta.
