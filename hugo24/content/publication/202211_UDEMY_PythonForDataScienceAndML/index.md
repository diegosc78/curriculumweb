---
title: "Python for Data Science and Machine Learning Bootcamp"
authors:
- admin
date: "2022-12-08T00:00:00Z"
doi: ""
draft: false

# Schedule page publish date (NOT publication's date).
publishDate: "2022-12-08T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: []

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: "Python Data Science Bootcamp"

abstract: Python for Data Science and Machine Learning Bootcamp

# Summary. An optional shortened abstract.
summary: Python for Data Science and Machine Learning Bootcamp - UDEMY - 2022

tags:
- IA
featured: false

links:
- name: Enlace al curso
  url: "https://www.udemy.com/course/python-for-data-science-and-machine-learning-bootcamp/"
url_pdf: ''
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ''
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

## Datos del curso

* Organismo: UDEMY
* Fecha: 2022-12
* Duración: 25h
* Modalidad: teleformación
* Diploma acreditativo: disponible

## Temario

* Programming with Python
* NumPy with Python
* Using pandas Data Frames to solve complex tasks
* Use pandas to handle Excel Files
* Web scraping with python
* Connect Python to SQL
* Use matplotlib and seaborn for data visualizations
* Use plotly for interactive visualizations
* Machine Learning with SciKit Learn, including:
* Linear Regression
* K Nearest Neighbors
* K Means Clustering
* Decision Trees
* Random Forests
* Natural Language Processing
* Neural Nets and Deep Learning
* Support Vector Machines
* Recommender Systems
* Big Data and Spark with Python
