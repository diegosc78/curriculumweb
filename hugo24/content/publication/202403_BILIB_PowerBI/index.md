---
title: "Análisis y visualización de datos con Excel y PowerBI"
authors:
- admin
date: "2024-03-27T00:00:00Z"
doi: ""
draft: false

# Schedule page publish date (NOT publication's date).
publishDate: "2024-03-27T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: []

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: "Análisis de datos con Excel y PowerBI"

abstract: Análisis y visualización de datos con Excel y PowerBI

# Summary. An optional shortened abstract.
summary: Análisis y visualización de datos con Excel y PowerBI - Centro de Desarrollo de Competencias Digitales de Castilla-La Mancha - 2024

tags:
- BI
featured: false

links:
- name: Entidad organizadora
  url: "https://formados.bilib.es/"
url_pdf: ''
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ''
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

## Datos del curso

* Organismo: Centro de Desarrollo de Competencias Digitales de Castilla-La Mancha
* Fecha: 2024-03
* Duración: 80h
* Modalidad: teleformación
* Diploma acreditativo: disponible

## Temario

* Introducción a la inteligencia de negocio y herramientas
* ETL en Excel - Power Query y PowerBI
* Diseño del modelo de datos
* Transformación de datos en Power Query y Power BI
* Visualizaciones de alto impacto
* Potenciar los informes con DAX
* Integración de Excel y PowerBI
* Compartir PowerBI con los usuarios
* El servicio de PowerBI en la nube
* Business Case: Dashboard para monitorizar KPIs
