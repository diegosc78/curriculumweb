---
title: "Introducción a inteligencia artificial"
authors:
- admin
date: "2022-09-02T00:00:00Z"
doi: ""
draft: false

# Schedule page publish date (NOT publication's date).
publishDate: "2022-09-02T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: []

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: "Intro IA"

abstract: Introducción a inteligencia artificial

# Summary. An optional shortened abstract.
summary: Introducción a inteligencia artificial - FormadoresIT - 2022

tags:
- IA
featured: false

links:
- name: Entidad organizadora
  url: "https://formadoresit.es/"
url_pdf: ''
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ''
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

## Datos del curso

* Organismo: Formadores IT
* Fecha: 2022-09
* Duración: 30h
* Modalidad: telepresencial
* Diploma acreditativo: disponible

## Temario

* Introducción a la Inteligencia Artificial
* Transformación Digital e Inteligencia Artificial
* Robótica y Automatización
* Tecnología e Innovaciones asociadas a la IA
* Ámbitos de aplicación
* Recomendaciones
* Futuro de la IA
* Liderazgo y Gestión de proyectos de inteligencia artificial
