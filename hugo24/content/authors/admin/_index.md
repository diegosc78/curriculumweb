---
# Display name
title: Diego Souto

# Full name (for SEO)
first_name: Diego
last_name: Souto Catoira

# Status emoji
status:
  icon: ☕️

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Gerente de proyectos. Responsable área estrategia tecnológica

# Organizations/Affiliations to show in About widget
organizations:
  - name: Altia
    url: https://www.altia.es/

# Short bio (displayed in user profile at end of posts)
bio: Continuamente aprendiendo cosas. Me encanta cacharrear en casa.

# Interests to show in About widget
interests:
  - Viajar
  - IoT. Domótica
  - SBCs. Raspberries
  - Kubernetes
  - Arquitecturas de integración de sistemas
  - IaC. CI/CD. Automatización

# Education to show in About widget
education:
  courses:
    - course: Ingeniero Técnico en Informática de Gestión
      institution: Universidad de A Coruña
      year: 1999
    - course: Ingeniero en Informática
      institution: Universidad de A Coruña
      year: 2003

# Skills
# For available icons, see: https://docs.hugoblox.com/getting-started/page-builder/#icons
skills:
  - name: Técnicas
    items:
      - name: Kubernetes
        description: ''
        percent: 70
        icon: docker
        icon_pack: fab
      - name: Linux
        description: ''
        percent: 70
        icon: linux
        icon_pack: fab
      - name: Smart
        description: ''
        percent: 70
        icon: wifi
        icon_pack: fas
      - name: EAI-SOA
        description: ''
        percent: 70
        icon: link
        icon_pack: fas
      - name: Java
        description: ''
        percent: 70
        icon: java
        icon_pack: fab
      - name: Javascript
        description: ''
        percent: 60
        icon: js
        icon_pack: fab
      - name: Smart Cards
        description: ''
        percent: 70
        icon: credit-card
        icon_pack: fas
      - name: Blockchain
        description: ''
        percent: 30
        icon: bitcoin
        icon_pack: fab
      - name: Machine learning
        percent: 30
        icon: brain
        icon_pack: fas
  - name: Personales
    color: '#eeac02'
    color_border: '#f0bf23'
    items:
      - name: Organización
        description: ''
        percent: 90
        icon: sitemap
        icon_pack: fas
      - name: Comunicación
        description: ''
        percent: 80
        icon: comments
        icon_pack: fas
      - name: Liderazgo
        description: ''
        percent: 80
        icon: people-roof
        icon_pack: fas
      - name: Gestión de proyectos
        description: ''
        percent: 90
        icon: tasks
        icon_pack: fas
      - name: Proactividad
        description: ''
        percent: 80
        icon: heart
        icon_pack: fas
      - name: Mejora continua
        description: ''
        percent: 90
        icon: arrows-up-to-line
        icon_pack: fas
      - name: Orientación a cliente
        description: ''
        percent: 80
        icon: user-tie
        icon_pack: fas
      - name: Jugador de equipo
        description: ''
        percent: 80
        icon: people-group
        icon_pack: fas
      - name: Aprendizaje continuo
        description: ''
        percent: 90
        icon: book-open-reader
        icon_pack: fas

# Social/Academic Networking
# For available icons, see: https://docs.hugoblox.com/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
  - icon: envelope
    icon_pack: fas
    link: '/#contact'
  - icon: linkedin
    icon_pack: fab
    link: https://www.linkedin.com/in/diegosouto
  - icon: bitbucket
    icon_pack: fab
    link: https://bitbucket.org/diegosc78
  - icon: github
    icon_pack: fab
    link: https://github.com/diegosc78
  - icon: docker
    icon_pack: fab
    link: https://hub.docker.com/u/ponte124
  # Link to a PDF of your resume/CV.
  # To use: copy your resume to `static/uploads/resume.pdf`, enable `ai` icons in `params.yaml`,
  # and uncomment the lines below.
  - icon: cv
    icon_pack: ai
    link: uploads/CV_DiegoSoutoCatoira.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: 'diego.souto@gmail.com'

# Highlight the author in author lists? (true/false)
highlight_name: true
---

Soy un gerente de proyectos en Altia, que ha trabajado durante años en proyectos de todo tipo: consultoría, desarrollo de software, implantación de sistemas, soporte-mantenimiento,... que dedica una parte importante de su tiempo ahora al asesoramiento, preventa y elaboración de propuestas de valor y que participa activamente en procesos y órganos de mejora interna de la compañía, tanto de calidad como tecnológicas o de estrategia.

Actualmente también colaboro transversalmente dentro de la Unidad de Riesgos Globales del Grupo Altia y dentro de la Dirección de Tecnología de Altia como Responsable del Área de Estrategia Tecnológica y Partnerships. En el pasado también he colaborado en otros ámbitos transversales: herramientas corporativas, responsable de formación, mejora de procesos

Siempre aprendiendo cosas nuevas e intentando transmitir a mis equipos valores de profesionalidad, responsabilidad y pasión por las cosas bien hechas.
{style="text-align: justify;"}
