---
title: Soporte-mantenimiento de maestros financieros
summary: Servicio de soporte-mantenimiento de múltiples sistemas maestros financieros
tags:
- Apps web
date: "2016-09-01T00:00:00Z"
external_link: ""
image:
  caption: ""
  focal_point: Smart
links: []
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---
* **Objetivo:** Servicio de soporte-mantenimiento de múltiples sistemas maestros financieros
* **Mi rol en el proyecto:** Jefe de proyecto
* **Tipo de proyecto:** soporte-mantenimiento de software
* **Estado:** en mantenimiento
* **Cliente:** multinacional del sector textil
* **Tecnologías:** Java, Spring, GWT, HTML-CSS, SWT, AS400,...
* **Entorno funcional:** Gestión de proveedores, gestión de impuestos, países, ...
