---
title: Implantación, soporte y mantenimiento ESB
summary: Implantación y posterior soporte-mantenimiento de una plataforma SOA, principalemente un Enterprise Service Bus (ESB)
tags:
- SOA
date: "2016-09-01T00:00:00Z"
external_link: ""
image:
  caption: ""
  focal_point: Smart
links: []
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---
* **Objetivo:** Implantación y posterior soporte-mantenimiento de una plataforma SOA, principalemente un Enterprise Service Bus (ESB)
* **Descripción:** Incluye también disponibilización de servicios, flujos de auditoría, failover, balanceo, throttling, enrutamiento de mensajes, ...
* **Mi rol en el proyecto:** Jefe de proyecto
* **Tipo de proyecto:** implantación de sistemas; soporte-mantenimiento de sistemas
* **Estado:** en mantenimiento
* **Cliente:** administración pública local
* **Tecnologías:** WSO2 ESB, WSO2 Message Broker ...
* **Entorno funcional:** tecnologías de la información
