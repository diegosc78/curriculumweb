---
title: Sistemas corporativos
summary: Soporte-mantenimiento de múltiples sistemas corporativos
tags:
- DevOps
- Contenedores
date: "2017-02-02T00:00:00Z"
external_link: ""
image:
  caption: ""
  focal_point: ""
links: 
- url: "https://www.altia.es"
  name: Sistemas corporativos Altia
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---
* **Objetivo:** Soporte-mantenimiento de varios sistemas corporativos
* **Mi rol en el proyecto:** Jefe de proyecto. DevOps.
* **Tipo de proyecto:** soporte e implantación de soluciones
* **Estado:** en mantenimiento
* **Cliente:** proyecto interno (Altia)
* **Componentes:**
  * Plataforma de orquestación de contenedores
  * Repositorio de artefactos
  * Integración continua
  * Análisis estático de código
  * Análisis de vulnerabilidades en dependencias
  * Registry de imágenes de contenedores
  * eLearning
  * Encuestas
  * Inventario
  * CRM
  * Analítica y ETLs
* **Tecnologías:** Kubernetes, Sonatype Nexus, Jenkins, Sonarqube, Dependency-Track, Harbor, Moodle, LimeSurvey, SnipeIT, Metabase, Talend, ...
* **Entorno funcional:** consultoría tic y desarrollo
