---
title: Registro y declaración del IVA
summary: Desarrollo de un sistema de gestión para las declaraciones de IVA a agencias tributarias europeas, integrando información de los clientes y automatizando la integración con las agencias tributarias, tanto en suministro inmediato como en declaraciones periódicas según calendario del contribuyente, mediante API y RPA
tags:
- Apps web
- RPA
- BPM
- Contenedores
- Cloud
date: "2020-09-01T00:00:00Z"
external_link: ""
image:
  caption: ""
  focal_point: Smart
links:
  - url: https://vatify.marosavat.com/
    name: Vatify
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---
* **Objetivo:** Desarrollo de un sistema de gestión para las declaraciones de IVA a agencias tributarias europeas
* **Mi rol en el proyecto:** Gerente de proyecto
* **Tipo de proyecto:** desarrollo e implantación de sistemas de información
* **Estado:** en ejecución
* **Cliente:** gestoría especializada en IVA a nivel internacional
* **Tecnologías:** Java, Spring boot, HTML5-CSS3, jHipster, Angular, Camunda, UIPath, ...
* **Entorno funcional:** Gestión de IVA