---
title: Desarrollo e implantación portal de producto
summary: Desarrollo, maquetación e integraciones de portal de producto y área privada de seguimiento de pedidos
tags:
- CMS
date: "2020-01-01T00:00:00Z"
external_link: ""
image:
  caption: ""
  focal_point: Smart
links: 
- url: "https://mimmetic.design"
  name: Portal
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---
* **Objetivo:** Desarrollo, maquetación e integraciones de portal de producto y área privada de seguimiento de pedidos
* **Mi rol en el proyecto:** Gerente de proyecto
* **Tipo de proyecto:** desarrollo e implantación de sistemas de información
* **Estado:** finalizaod
* **Cliente:** empresa sector industrial madera
* **Tecnologías:** Drupal, HTML5-CSS3, PHP, Javascript, Angular
* **Entorno funcional:** portal corporativo, catálogo de productos
