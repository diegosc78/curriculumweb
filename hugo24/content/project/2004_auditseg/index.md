---
title: Auditoría de seguridad
summary: Consultoría tecnológica para la auditoría de seguridad de la red de datos y red ISP
tags:
- Consultoría
date: "2004-09-01T00:00:00Z"
external_link: ""
image:
  caption: ""
  focal_point: Smart
links: []
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---
* **Objetivo:** Auditoría de seguridad
* **Descripción:** Consultoría tecnológica para la auditoría de seguridad de la red de datos y red ISP
* **Mi rol en el proyecto:** Consultor de sistemas
* **Tipo de proyecto:** consultoría
* **Estado:** finalizado
* **Cliente:** telco
* **Entorno funcional:** sistemas de información y comunicaciones
