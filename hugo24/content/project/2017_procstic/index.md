---
title: Procedimientos y herramientas gestión área TIC
summary: Consultoría para mejora procesos de gestión del área TIC e implantación de una herramienta de gestión de peticiones
tags:
- Consultoría
date: "2017-09-01T00:00:00Z"
external_link: ""
image:
  caption: ""
  focal_point: Smart
links: []
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---
* **Objetivo:** Consultoría para mejora procesos de gestión del área TIC e implantación de una herramienta de gestión de peticiones
* **Mi rol en el proyecto:** Jefe de proyecto
* **Tipo de proyecto:** implantación de sistemas; consultoría
* **Estado:** finalizado
* **Cliente:** organismo dependiente de la administración autonómica
* **Tecnologías:** Redmine
* **Entorno funcional:** tecnologías de la información