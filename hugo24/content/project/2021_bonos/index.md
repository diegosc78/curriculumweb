---
title: Desarrollo e implantación de un sistema de gestión de bonos sociales
summary: Desarrollo e implantación de un sistema de gestión de bonos sociales para ayudas de emergencia de alimentación en una entidad local, junto con una cadena de supermercados como colaborador, utilizando una tarjeta
tags:
- Apps web
- SOA
- Cloud
date: "2021-09-01T00:00:00Z"
external_link: ""
image:
  caption: ""
  focal_point: Smart
links: []
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---
* **Objetivo:** Desarrollo e implantación de un sistema de gestión de bonos sociales
* **Descripción:** Desarrollo e implantación de un sistema de gestión de bonos sociales para ayudas de emergencia de alimentación en una entidad local, junto con una cadena de supermercados como colaborador, utilizando una tarjeta
* **Mi rol en el proyecto:** Gerente de proyecto
* **Tipo de proyecto:** desarrollo-mantenimiento
* **Estado:** en soporte-mantenimiento
* **Cliente:** administración local
* **Tecnologías:** Java, Spring boot, HTML5-CSS3, jHipster, Angular, ...
* **Entorno funcional:** Gestión de expedientes, servicios sociales, gestión de ayudas.
