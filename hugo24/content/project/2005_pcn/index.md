---
title: Plan de continuidad de negocio
summary: Consultoría tecnológica para la definición de un plan de contingencia y continuidad de negocio
tags:
- Consultoría
date: "2005-09-01T00:00:00Z"
external_link: ""
image:
  caption: ""
  focal_point: Smart
links: []
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---
* **Objetivo:** Plan de continuidad de negocio
* **Descripción:** Consultoría tecnológica para la definición de un plan de contingencia y continuidad de negocio
* **Mi rol en el proyecto:** Consultor de sistemas
* **Tipo de proyecto:** consultoría
* **Estado:** finalizado
* **Cliente:** gran empresa sector turístico
* **Entorno funcional:** tecnologías de la información
