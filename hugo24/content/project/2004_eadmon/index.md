---
title: Desarrollo-Mantenimiento de sistema de gestión de expedientes
summary: Desarrollo-mantenimiento de una plataforma de gestión de expedienes para la administración pública
tags:
- Apps web
- e-Admon
date: "2004-09-01T00:00:00Z"
external_link: ""
image:
  caption: ""
  focal_point: Smart
links:
  - url: "https://www.altia.es/"
    name: "Altia"
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---
* **Objetivo:** Gestión de expedientes
* **Descripción:** Desarrollo-mantenimiento de una plataforma de gestión de expedienes para la administración pública
* **Mi rol en el proyecto:** Analista-Programador
* **Tipo de proyecto:** desarrollo-mantenimiento de sistemas de información
* **Estado:** en mantenimiento
* **Cliente:** varios organismos públicos de ámbito local
* **Entorno funcional:** gestión de expedientes
