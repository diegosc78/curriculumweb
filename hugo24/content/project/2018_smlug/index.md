---
title: Lugo Smart
summary: Desarrollo de la iniciativa Lugo Smart City
tags:
- Smart cities
- IoT
- GIS
- BI
- SOA
- Apps web
- PoS
- CMS
- Gobierno abierto
- Contenedores
date: "2018-06-06T00:00:00Z"
external_link: ""
image:
  caption: Nueva sede
  focal_point: Smart
links: 
- url: "https://www.lugo.gal"
  name: Nueva sede
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---
* **Objetivo:** Desarrollo de la iniciativa Lugo Smart City
* **Mi rol en el proyecto:** Gerente de proyecto (varios componentes)
* **Tipo de proyecto:** desarrollo e implantación de sistemas de información
* **Estado:** en mantenimiento
* **Cliente:** administración local
* **Componentes:**
  * Sistema de administración electrónica, incluyendo licitación electrónica
  * GIS
  * Nuevos servicios para la tarjeta ciudadana
  * Parking inteligente
  * Smart waste
  * Eficiencia energética alumbrado, edificios públicos y telemedida contadores de agua
  * Consultoría plataforma smart
  * Infraestructura de servidores y sistemas base
* **Tecnologías:** Python, PHP, Drupal, GeoServer, PC/SC, OTEA,...
* **Entorno funcional:** Gestión de expedientes, Gobierno abierto, Smart city
