---
title: Coruña Smart City
summary: Desarrollo e implantación de la plataforma para Coruña Smart City
tags:
- Smart cities
- IoT
- GIS
- BI
- SOA
- Apps web
- PoS
- CMS
- Gobierno abierto
date: "2014-06-06T00:00:00Z"
external_link: ""
image:
  caption: Portal CSC
  focal_point: Smart
links: 
- url: "https://smart.coruna.es/"
  name: Portal CSC
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---
* **Objetivo:** Desarrollo e implantación de una plataforma Smart City
* **Mi rol en el proyecto:** Jefe de proyecto (plataforma)
* **Tipo de proyecto:** desarrollo e implantación de sistemas de información
* **Estado:** finalizado
* **Cliente:** administración local
* **Componentes:**
  * Plataforma de interoperabilidad
  * Sistema de interconexión sensórica
  * Red troncal y gateway IoT
  * GIS
  * Visualización avanzada
  * Business Intelligence
  * Portal smart
  * Open Data
* **Tecnologías:** Java, Spring, Hibernate, HTML5-CSS3, bootstrap, MQTT, Oracle Service Bus, Sofia2, ArcGIS, eVidens, Liferay,...
* **Entorno funcional:** Smart city
