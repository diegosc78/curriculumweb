---
title: Tarjeta Coruña Millennium
summary: Desarrollo e implantación de un sistema de tarjeta ciudadana
tags:
- Apps web
- e-Admon
- Smart Cards
date: "2005-09-01T00:00:00Z"
external_link: ""
image:
  caption: ""
  focal_point: Smart
links:
  - url: "https://www.coruna.gal/"
    name: "Ayuntamiento de A Coruña"
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---
* **Objetivo:** Desarrollo e implantación de un sistema de tarjeta ciudadana
* **Descripción:** Un sistema multiservicio con más de 200.000 tarjetas incluyendo servicios de monedero electrónico para transportes, ORA, pago de tasas, identificación en bibliotecas, en deportes, ... abarcando todo el ciclo de vida de las tarjetas.
* **Mi rol en el proyecto:** Jefe de proyecto
* **Tipo de proyecto:** desarrollo de sistemas de información
* **Estado:** en mantenimiento
* **Cliente:** administración local
* **Tecnologías:** Java, Hibernate, HTML-CSS, PC/SC, Quartz,...
* **Entorno funcional:** tarjeta ciudadana
