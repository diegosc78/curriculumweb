---
title: Mercurio
summary: Evolución del sistema de licitación electrónica Mercurio
tags:
- Apps web
- e-Admon
date: "2011-09-01T00:00:00Z"
external_link: ""
image:
  caption: ""
  focal_point: Smart
links:
  - url: https://mercurio.altia.es/
    name: Producto en SaaS
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---
* **Objetivo:** Desarrollo y mantenimiento de un sistema de licitación electrónica
* **Descripción:** Soporte al proceso de publicación de anuncios, invitaciones, presentación de ofertas (incluyendo ensobrado y lacrado electrónico), gestión de las aperturas por las mesas de contratación, adjudicación, firma de contrato, registro de contratistas, etc. 
Se trata de un producto desplegado on-premise y on-cloud (SaaS)
* **Mi rol en el proyecto:** Jefe de proyecto
* **Tipo de proyecto:** desarrollo-mantenimiento de sistemas de información
* **Estado:** en mantenimiento
* **Cliente:** múltiples administraciones públicas: locales, autonómicas y estatales
* **Tecnologías:** Java, Spring, Hibernate, HTML5-CSS3, PKI, ...
* **Entorno funcional:** licitación electrónica
