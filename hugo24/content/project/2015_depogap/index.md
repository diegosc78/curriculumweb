---
title: depoGAP
summary: Gestión de activos provinciales
tags:
- Smart cities
- IoT
- GMAO
- GIS
- BI
- CMS
- Apps web
date: "2015-06-06T00:00:00Z"
external_link: ""
image:
  caption: Cuadro de Mando Integral
  focal_point: Smart
links: []
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---

* **Objetivo:** Desarrollo e implantación de una plataforma de gestión de activos provinciales
* **Mi rol en el proyecto:** Jefe de proyecto (plataforma, cmi, portal)
* **Tipo de proyecto:** desarrollo e implantación de sistemas de información
* **Estado:** finalizado
* **Cliente:** administración provincial
* **Componentes:**
  * Plataforma software de gestión de activos
  * Inventario georrefenciado de activos (más de 2 millones en 60 ayuntamientos)
  * Cuadro de mando integral
  * Portal web y app móvil al ciudadano
  * Pilotos de eficiencia energética: en instalaciones deportivas, en edificios públicos y en hogares
* **Tecnologías:** Java, FAMA, HTML5-CSS3, Sofia2, ArcGIS, eVidens, Liferay,...
* **Entorno funcional:** Inventario georreferenciado de bienes, Smart city
