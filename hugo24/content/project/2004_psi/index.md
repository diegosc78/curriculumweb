---
title: Plan estratégico de sistemas
summary: Consultoría tecnológica para la definición de un plan estratégico de evolución de los sistemas corporativos
tags:
- Consultoría
date: "2004-09-01T00:00:00Z"
external_link: ""
image:
  caption: ""
  focal_point: Smart
links: []
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---
* **Objetivo:** Plan estratégico de sistemas
* **Descripción:** Consultoría tecnológica para la definición de un plan estratégico de evolución de los sistemas corporativos
* **Mi rol en el proyecto:** Consultor
* **Tipo de proyecto:** consultoría
* **Estado:** finalizado
* **Cliente:** organismo dependiente de la administración autonómica
* **Entorno funcional:** gestión de emergencias sanitarias, tecnologías de la información
