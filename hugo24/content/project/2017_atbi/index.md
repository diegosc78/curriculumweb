---
title: Servicio de asistencia técnica Business Intelligence
summary: Servicio de apoyo técnico en la construcción y mantenimiento de cuadros de mando, informes e indicadores
tags:
- BI
- Contenedores
date: "2017-09-01T00:00:00Z"
external_link: ""
image:
  caption: ""
  focal_point: Smart
links: []
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---
* **Objetivo:** Servicio de apoyo técnico en la construcción y mantenimiento de cuadros de mando, informes e indicadores
* **Mi rol en el proyecto:** Gerente de proyecto
* **Tipo de proyecto:** asistencia técnica
* **Estado:** en mantenimiento
* **Cliente:** empresa sector industrial
* **Tecnologías:** Airflow, Nifi, Clickhouse, Pentaho, PowerBI ...
* **Entorno funcional:** Gambling
