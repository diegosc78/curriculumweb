---
title: Implantación, soporte y mantenimiento plataforma IoT y SOA
summary: Implantación y posterior soporte-mantenimiento de una plataforma IoT y SOA para el sector industrial facilitando el streaming y procesamiento de eventos en tiempo real así como la orquestación de servicios y publicación de APIs
tags:
- SOA
- IoT
- Contenedores
date: "2020-09-01T00:00:00Z"
external_link: ""
image:
  caption: ""
  focal_point: Smart
links: []
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---
* **Objetivo:** Implantación y posterior soporte-mantenimiento de una plataforma IoT y SOA
* **Mi rol en el proyecto:** Jefe de proyecto
* **Tipo de proyecto:** implantación de sistemas; soporte-mantenimiento de sistemas
* **Estado:** en soporte-mantenimiento
* **Cliente:** empresa sector industrial
* **Tecnologías:** Kubernetes, WSO2 Enterprise Integrator, WSO2 Identity Server, WSO2 Stream Processor, WSO2 API Manager, RabbitMQ, MongoDB, ...
* **Entorno funcional:** monitorización industrial
