---
title: Tarjeta Ciudadana Logroño
summary: Desarrollo e implantación de un sistema de tarjeta ciudadana
tags:
- Apps web
- e-Admon
- Smart Cards
date: "2008-09-01T00:00:00Z"
external_link: ""
image:
  caption: ""
  focal_point: Smart
links: []
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
slides: ""
---
* **Objetivo:** Desarrollo e implantación de un sistema de tarjeta ciudadana
* **Descripción:** Tarjeta multiservicio inalámbrica, incluyendo monedero electrónico y gestión de puntos de recarga
* **Mi rol en el proyecto:** Arquitecto de software
* **Tipo de proyecto:** desarrollo de sistemas de información
* **Estado:** finalizado
* **Cliente:** administración local
* **Tecnologías:** Java, Hibernate, HTML-CSS, PC/SC, Quartz,...
* **Entorno funcional:** tarjeta ciudadana
